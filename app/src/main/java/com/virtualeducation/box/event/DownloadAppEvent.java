package com.virtualeducation.box.event;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.virtualeducation.box.model.AppModel;

public class DownloadAppEvent {
    public final AppModel appModel;
    public final ImageView icDownload;
    public final ProgressBar progressBar;
    public final TextView textDownloadProgress;
    public final boolean fromDialog;
    public final int holderPosition;

    public DownloadAppEvent(AppModel appModel, ImageView icDownload,TextView textDownloadProgress,ProgressBar progressBar, boolean fromDialog, int holderPosition){
        this.appModel=appModel;
        this.icDownload=icDownload;
        this.textDownloadProgress=textDownloadProgress;
        this.progressBar=progressBar;
        this.fromDialog = fromDialog;
        this.holderPosition = holderPosition;
    }
}
