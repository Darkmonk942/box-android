package com.virtualeducation.box.event;

import com.virtualeducation.box.model.AppModel;

public class GetActivationKeyEvent {
    public final int appId;
    public final AppModel appModel;

    public GetActivationKeyEvent(int appId, AppModel appModel){
        this.appId=appId;
        this.appModel = appModel;
    }
}
