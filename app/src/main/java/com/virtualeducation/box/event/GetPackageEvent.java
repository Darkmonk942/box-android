package com.virtualeducation.box.event;

import com.virtualeducation.box.model.AppCategoryModel;
import com.virtualeducation.box.model.AppModel;
import com.virtualeducation.box.model.PackageRetailModel;

public class GetPackageEvent {
    public final PackageRetailModel packageRetailModel;

    public GetPackageEvent(PackageRetailModel packageRetailModel){
        this.packageRetailModel=packageRetailModel;
    }
}
