package com.virtualeducation.box.event;

import com.virtualeducation.box.model.FeedbackModel;

public class FeedbackSendEvent {
    public final FeedbackModel feedbackModel;

    public FeedbackSendEvent(FeedbackModel feedbackModel){
        this.feedbackModel=feedbackModel;
    }
}
