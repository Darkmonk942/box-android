package com.virtualeducation.box.event;

public class ProgressDownloadEvent {
    public final long currentBytes;
    public final long totalBytes;
    public boolean isDone;

    public ProgressDownloadEvent(long currentBytes,long totalBytes,boolean isDone){
        this.currentBytes=currentBytes;
        this.totalBytes=totalBytes;
        this.isDone=isDone;
    }
}
