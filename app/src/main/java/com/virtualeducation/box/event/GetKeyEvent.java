package com.virtualeducation.box.event;

import com.virtualeducation.box.model.AppModel;

public class GetKeyEvent {
    public final AppModel appModel;
    public final int appId;
    public final boolean isTaken;
    public final String key;

    public GetKeyEvent(AppModel appModel, int appId,boolean isTaken,String key){
        this.appModel=appModel;
        this.appId=appId;
        this.isTaken=isTaken;
        this.key=key;
    }
}
