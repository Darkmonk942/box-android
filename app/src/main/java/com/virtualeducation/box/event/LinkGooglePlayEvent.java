package com.virtualeducation.box.event;

public class LinkGooglePlayEvent {
    public final String url;

    public LinkGooglePlayEvent(String url) {
        this.url = url;
    }
}
