package com.virtualeducation.box.event;

public class BarCodeScanEvent {
    public final String barcode;

    public BarCodeScanEvent(String barcode) {
        this.barcode = barcode;
    }
}
