package com.virtualeducation.box.event;

import android.app.AlertDialog;

public class AlertDialogEvent {
    public static final String DIALOG_GET_KEY="get_key";
    public static final String DIALOG_SHOW_KEY="show_key";
    public static final String DIALOG_MENU="menu";
    public static final String DIALOG_APP="app";

    public final String eventType;

    public AlertDialogEvent(String eventType){
        this.eventType=eventType;
    }
}
