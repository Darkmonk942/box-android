package com.virtualeducation.box.event;

import com.virtualeducation.box.model.AppCategoryModel;
import com.virtualeducation.box.model.AppModel;

public class AboutAppEvent {
    public final AppModel appModel;
    public final AppCategoryModel appCategoryModel;
    public int holderPosition;

    public AboutAppEvent(AppModel appModel, AppCategoryModel appCategoryModel, int holderPosition){
        this.appModel=appModel;
        this.appCategoryModel=appCategoryModel;
        this.holderPosition = holderPosition;
    }
}
