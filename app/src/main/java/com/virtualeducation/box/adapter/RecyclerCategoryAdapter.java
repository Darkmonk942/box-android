package com.virtualeducation.box.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.virtualeducation.box.R;
import com.virtualeducation.box.event.AboutAppEvent;
import com.virtualeducation.box.event.DownloadAppEvent;
import com.virtualeducation.box.event.GetKeyEvent;
import com.virtualeducation.box.event.LinkGooglePlayEvent;
import com.virtualeducation.box.event.ProgressDownloadEvent;
import com.virtualeducation.box.model.AppCategoryModel;
import com.virtualeducation.box.model.AppModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class RecyclerCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private final ArrayList<AppCategoryModel> categories;

    public RecyclerCategoryAdapter(Context context,ArrayList<AppCategoryModel> categories){
        this.categories = categories;
        this.context=context;
    }

    class ViewHolderPaid extends RecyclerView.ViewHolder {
        TextView categoryName;
        TextView textDownloadProgress;
        TextView textMyKey;
        ImageView icDownload;
        ImageButton btnInfo;
        ViewPager pagerApps;
        TabLayout tabLayout;
        FrameLayout frameDownload;
        FrameLayout frameGooglePlay;
        FrameLayout frameKey;

        ViewHolderPaid(View itemLayoutView) {
            super(itemLayoutView);
            categoryName=(TextView)itemLayoutView.findViewById(R.id.text_category);
            textDownloadProgress=(TextView)itemLayoutView.findViewById(R.id.text_download_progress);
            textMyKey=(TextView)itemLayoutView.findViewById(R.id.text_my_key);
            icDownload=(ImageView) itemLayoutView.findViewById(R.id.ic_download);
            btnInfo=(ImageButton) itemLayoutView.findViewById(R.id.btn_info);
            pagerApps=(ViewPager) itemLayoutView.findViewById(R.id.pager_apps);
            tabLayout= (TabLayout) itemLayoutView.findViewById(R.id.pager_tabs);
            frameDownload=(FrameLayout) itemLayoutView.findViewById(R.id.frame_download);
            frameGooglePlay=(FrameLayout) itemLayoutView.findViewById(R.id.frame_google_play);
            frameKey=(FrameLayout) itemLayoutView.findViewById(R.id.frame_key);
        }
    }

    class ViewHolderFree extends RecyclerView.ViewHolder {
        TextView categoryName;
        TextView textProgress1;
        TextView textProgress2;
        ImageView icDownload1;
        ImageView icDownload2;
        ImageView icGooglePlay2;
        ImageButton btnInfo;
        ViewPager pagerApps;
        TabLayout tabLayout;
        FrameLayout frameDownload1;
        FrameLayout frameDownload2;
        FrameLayout frameGooglePlay1;
        FrameLayout frameGooglePlay2;

        ViewHolderFree(View itemLayoutView) {
            super(itemLayoutView);
            textProgress1=(TextView)itemLayoutView.findViewById(R.id.text_progress_1);
            textProgress2=(TextView)itemLayoutView.findViewById(R.id.text_progress_2);
            categoryName=(TextView)itemLayoutView.findViewById(R.id.text_category);
            btnInfo=(ImageButton) itemLayoutView.findViewById(R.id.btn_info);
            pagerApps=(ViewPager) itemLayoutView.findViewById(R.id.pager_apps);
            tabLayout= (TabLayout) itemLayoutView.findViewById(R.id.pager_tabs);
            icDownload1=(ImageView) itemLayoutView.findViewById(R.id.ic_download_1);
            icDownload2=(ImageView) itemLayoutView.findViewById(R.id.ic_download_2);
            icGooglePlay2 = (ImageView) itemLayoutView.findViewById(R.id.ic_google_play_2);
            frameDownload1=(FrameLayout) itemLayoutView.findViewById(R.id.frame_download_1);
            frameDownload2=(FrameLayout) itemLayoutView.findViewById(R.id.frame_download_2);
            frameGooglePlay1=(FrameLayout) itemLayoutView.findViewById(R.id.frame_google_play_1);
            frameGooglePlay2=(FrameLayout) itemLayoutView.findViewById(R.id.frame_google_play_2);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType){
            case 0:
                View paidLayout= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_app_paid,null);
                return new RecyclerCategoryAdapter.ViewHolderPaid(paidLayout);
            case 1:
                View freeLayout= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_app_free,null);
                return new RecyclerCategoryAdapter.ViewHolderFree(freeLayout);
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return categories.get(position).isPaid ? 0 : 1;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        AppCategoryModel categoryModel=categories.get(position);
        if(holder.getItemViewType()==0){ // Paid category
            ViewHolderPaid holderPaid=(ViewHolderPaid)holder;
            holderPaid.categoryName.setText(categoryModel.name);
            if(categoryModel.apps.size()==0){
                return;
            }

            holderPaid.frameDownload.setTag(getPaidDownloadEvent(categoryModel.apps.get(0),holderPaid));
            holderPaid.frameDownload.setOnClickListener(listenerDownloadApk);
            holderPaid.frameKey.setTag(categoryModel);

            initAppInfoBtn(holderPaid.btnInfo,holderPaid.pagerApps,categoryModel, holderPaid.getAdapterPosition());
            initListenerGetKey(holderPaid);
            initPaidAppPager(holderPaid,categoryModel);
            initGooglePlayBtnPaidApp(holderPaid.frameGooglePlay,holderPaid.pagerApps,categoryModel);
            isKeyAlreadyGot(holderPaid,categoryModel);

        }else{  // Free category
            ViewHolderFree holderFree=(ViewHolderFree)holder;
            holderFree.categoryName.setText(categoryModel.name);

            if(categoryModel.apps.size()==0){
                return;
            }
            if (categoryModel.apps.size()==1){
                holderFree.icDownload2.setVisibility(View.GONE);
                holderFree.icGooglePlay2.setVisibility(View.GONE);
            }

            holderFree.frameDownload1.setOnClickListener(listenerDownloadApk);
            holderFree.frameDownload2.setOnClickListener(listenerDownloadApk);
            initFreeAppPager(holderFree,holderFree.tabLayout,categoryModel.apps);
            initGooglePlayBtnFreeLeftApp(holderFree.frameGooglePlay1,holderFree.pagerApps,categoryModel);
            initGooglePlayBtnFreeRightApp(holderFree.frameGooglePlay2,holderFree.pagerApps,categoryModel);
            initDownloadFreeLeftApp(holderFree,categoryModel);


        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    private DownloadAppEvent getPaidDownloadEvent(AppModel appModel,ViewHolderPaid holderPaid){
        return new DownloadAppEvent(appModel,holderPaid.icDownload,holderPaid.textDownloadProgress,null, false, holderPaid.getAdapterPosition());
    }

    private OnClickListener listenerDownloadApk    =   new OnClickListener() {
        @Override
        public void onClick(View v) {
            EventBus.getDefault().post(v.getTag());
        }
    };

    private void initListenerGetKey(final ViewHolderPaid holderPaid){
        final AppCategoryModel appCategoryModel=(AppCategoryModel)holderPaid.frameKey.getTag();
        holderPaid.frameKey.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                int appId=appCategoryModel.apps.get(holderPaid.pagerApps.getCurrentItem()).keyId;
                AppModel appModel=appCategoryModel.apps.get(holderPaid.pagerApps.getCurrentItem());
                if(appCategoryModel.isActivated){
                    String key=null;
                    if(appId==appCategoryModel.activationKeyModel.keyDBid){
                        key=appCategoryModel.activationKeyModel.key;
                    }
                    EventBus.getDefault().post(new GetKeyEvent(appModel,appId,appCategoryModel.isActivated,key));
                }else{
                    EventBus.getDefault().post(new GetKeyEvent(appModel,appId,appCategoryModel.isActivated,null));
                }
            }
        });
    }

    private void initPaidAppPager(final ViewHolderPaid holderPaid, final AppCategoryModel categoryModel){
        AppsListPaidAdapter paidAdapter=new AppsListPaidAdapter(context,categoryModel.apps,categoryModel, holderPaid.getAdapterPosition());
        holderPaid.pagerApps.setAdapter(paidAdapter);
        holderPaid.pagerApps.setOffscreenPageLimit(categoryModel.apps.size());
        holderPaid.tabLayout.setupWithViewPager(holderPaid.pagerApps);

        if (categoryModel.isActivated&&categoryModel.apps.size()>0){
            if(categoryModel.apps.get(0).keyId==categoryModel.activationKeyModel.keyDBid){
                holderPaid.textMyKey.setText(context.getResources().getString(R.string.main_text_show_key));
            }
        }

        holderPaid.pagerApps.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                holderPaid.btnInfo.setTag(categoryModel.apps.get(i));
                if (categoryModel.isActivated){
                    int keyDb=categoryModel.activationKeyModel.keyDBid;
                    if(categoryModel.apps.get(i).keyId==keyDb){
                        holderPaid.textMyKey.setText(context.getResources().getString(R.string.main_text_show_key));
                    }else{
                        holderPaid.textMyKey.setText("-");
                    }
                }
                holderPaid.frameDownload.setTag(getPaidDownloadEvent(categoryModel.apps.get(i),holderPaid));

                if(categoryModel.apps.get(i).localFilePath != null){
                    holderPaid.icDownload.setImageResource(R.drawable.ic_download_complete);
                }else{
                    holderPaid.icDownload.setImageResource(R.drawable.ic_download);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void isKeyAlreadyGot(ViewHolderPaid holderPaid, final AppCategoryModel categoryModel){
        if(categoryModel.apps.size()>0){
            if(categoryModel.apps.get(0).localFilePath != null){
                holderPaid.icDownload.setImageResource(R.drawable.ic_download_complete);
            }
        }

        if(categoryModel.isActivated){
            if(categoryModel.apps.size()>0){
                if(categoryModel.apps.get(0).keyId==categoryModel.activationKeyModel.keyDBid){
                    holderPaid.textMyKey.setText(context.getResources().getString(R.string.main_text_show_key));
                }else{
                    holderPaid.textMyKey.setText("-");
                }
            }
        }
    }

    private void initAppInfoBtn(ImageButton btnInfo, final ViewPager pagerApps, final AppCategoryModel categoryModel, final int position){
        btnInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new AboutAppEvent(categoryModel.apps.get(pagerApps.getCurrentItem()),categoryModel, position));
            }
        });
    }

    private void initFreeAppPager(final ViewHolderFree viewHolderFree, TabLayout tabLayout, final ArrayList<AppModel> apps){
        AppsListFreeAdapter freeAdapter=new AppsListFreeAdapter(context,apps);
        viewHolderFree.pagerApps.setAdapter(freeAdapter);
        viewHolderFree.pagerApps.setOffscreenPageLimit(apps.size());
        tabLayout.setupWithViewPager(viewHolderFree.pagerApps);

        viewHolderFree.pagerApps.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                AppModel appModelLeft = apps.get(i*2);
                if(appModelLeft.localFilePath != null){
                    viewHolderFree.icDownload1.setImageResource(R.drawable.ic_download_complete);
                }else{
                    viewHolderFree.icDownload1.setImageResource(R.drawable.ic_download);
                }
                if(i*2+1 < apps.size()){
                    viewHolderFree.icDownload2.setVisibility(View.VISIBLE);
                    viewHolderFree.icGooglePlay2.setVisibility(View.VISIBLE);
                    AppModel appModelRight = apps.get(i*2+1);
                    if(appModelRight.localFilePath != null){
                        viewHolderFree.icDownload2.setImageResource(R.drawable.ic_download_complete);
                    }else{
                        viewHolderFree.icDownload2.setImageResource(R.drawable.ic_download);
                    }
                }else{
                    viewHolderFree.icDownload2.setVisibility(View.GONE);
                    viewHolderFree.icGooglePlay2.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void initGooglePlayBtnPaidApp(FrameLayout frameGooglePlay, final ViewPager pagerApps, final AppCategoryModel categoryModel){
        frameGooglePlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new LinkGooglePlayEvent(categoryModel.apps.get(pagerApps.getCurrentItem()).urlGooglePlay));
            }
        });
    }

    private void initGooglePlayBtnFreeLeftApp(FrameLayout frameGooglePlay,final ViewPager pagerApps, final AppCategoryModel categoryModel){
        frameGooglePlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new LinkGooglePlayEvent(categoryModel.apps.get(pagerApps.getCurrentItem()*2).urlGooglePlay));
            }
        });
    }

    private void initGooglePlayBtnFreeRightApp(FrameLayout frameGooglePlay,final ViewPager pagerApps, final AppCategoryModel categoryModel){
        frameGooglePlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pagerApps.getCurrentItem()*2+1!=categoryModel.apps.size()){
                    EventBus.getDefault().post(new LinkGooglePlayEvent(categoryModel.apps.get(pagerApps.getCurrentItem()*2+1).urlGooglePlay));
                }
            }
        });
    }

    private void initDownloadFreeLeftApp(final ViewHolderFree holderFree, final AppCategoryModel categoryModel){

        if(categoryModel.apps.size() >0){
            if(categoryModel.apps.get(0).localFilePath != null){
                holderFree.icDownload1.setImageResource(R.drawable.ic_download_complete);
            }
        }
        if(categoryModel.apps.size() > 1){
            if(categoryModel.apps.get(1).localFilePath !=null){
                holderFree.icDownload2.setImageResource(R.drawable.ic_download_complete);
            }
        }

        holderFree.frameDownload1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AppModel appModel=categoryModel.apps.get(holderFree.pagerApps.getCurrentItem()*2);
                EventBus.getDefault().post(new DownloadAppEvent(appModel,holderFree.icDownload1,holderFree.textProgress1,null, false, 0));
            }
        });

        holderFree.frameDownload2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holderFree.pagerApps.getCurrentItem()*2+1 < categoryModel.apps.size()){
                    AppModel appModel=categoryModel.apps.get(holderFree.pagerApps.getCurrentItem()*2+1);
                    EventBus.getDefault().post(new DownloadAppEvent(appModel,holderFree.icDownload2,holderFree.textProgress2,null, false, 0));
                }
            }
        });
    }


}
