package com.virtualeducation.box.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.budiyev.android.codescanner.CodeScannerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.virtualeducation.box.R;
import com.virtualeducation.box.event.AboutAppEvent;
import com.virtualeducation.box.model.AppCategoryModel;
import com.virtualeducation.box.model.AppModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class AppsListPaidAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<AppModel> apps;
    private AppCategoryModel categoryModel;
    private int holderPosition;

    public AppsListPaidAdapter(Context context,ArrayList<AppModel> apps, AppCategoryModel categoryModel, int holderPosition){
        this.context=context;
        this.apps=apps;
        this.categoryModel = categoryModel;
        this.holderPosition = holderPosition;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        final AppModel appModel=apps.get(position);
        RelativeLayout layout = (RelativeLayout)LayoutInflater.from(collection.getContext()).inflate(R.layout.item_pager_app_paid,null);

        ImageView imgApp=(ImageView)layout.findViewById(R.id.img_app);
        imgApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new AboutAppEvent(appModel, categoryModel,holderPosition));
            }
        });

        Glide.with(context).load(appModel.urlLogo).into(imgApp);
        collection.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        return apps.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
