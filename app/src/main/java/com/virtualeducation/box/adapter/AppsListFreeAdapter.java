package com.virtualeducation.box.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.virtualeducation.box.R;
import com.virtualeducation.box.model.AppModel;

import java.util.ArrayList;

public class AppsListFreeAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<AppModel> apps;

    public AppsListFreeAdapter(Context context, ArrayList<AppModel> apps) {
        this.context = context;
        this.apps = apps;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        if (position != 0) {
            position = position * 2;
        }
        AppModel appModelLeft = apps.get(position);

        LinearLayout layout = (LinearLayout) LayoutInflater.from(collection.getContext()).inflate(R.layout.item_pager_app_free, null);

        ImageView icAppLeft = (ImageView) layout.findViewById(R.id.ic_app_left);
        ImageView icAppRight = (ImageView) layout.findViewById(R.id.ic_app_right);

        TextView textAppNameLeft = (TextView) layout.findViewById(R.id.text_app_name_left);
        TextView textAppNameRight = (TextView) layout.findViewById(R.id.text_app_name_right);

        RelativeLayout bgLayoutLeft = (RelativeLayout) layout.findViewById(R.id.layout_left);
        RelativeLayout bgLayoutRight = (RelativeLayout) layout.findViewById(R.id.layout_right);

        textAppNameLeft.setText(appModelLeft.appName);
        Glide.with(context).load(appModelLeft.urlIcon).into(icAppLeft);

        if (position + 1 != apps.size()) {
            AppModel appModelRight = apps.get(position + 1);
            textAppNameRight.setText(appModelRight.appName);
            if (appModelRight.bgColor != null) {
                bgLayoutRight.setBackgroundColor(Color.parseColor(appModelRight.bgColor.trim()));
            }
            Glide.with(context).load(appModelRight.urlIcon).into(icAppRight);
        }

        try {
            bgLayoutLeft.setBackgroundColor(Color.parseColor(appModelLeft.bgColor.trim()));
        } catch (Exception e) {
        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        if(apps.size() == 0){
            return 0;
        }
        if(apps.size()%2 == 0){
            return apps.size()/2;
        }else{
            return apps.size()/2 + 1;
        }
    }

    @Override
    public float getPageWidth(int position) {
        return 1f;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}