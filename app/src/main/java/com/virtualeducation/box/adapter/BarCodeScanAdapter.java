package com.virtualeducation.box.adapter;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.zxing.Result;
import com.virtualeducation.box.R;
import com.virtualeducation.box.event.BarCodeScanEvent;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.NonNull;

public class BarCodeScanAdapter extends PagerAdapter {
    private Context context;

    private CodeScannerView scannerView;
    public TextView textBarcodeError;
    public TextInputEditText editBarCode;
    public ProgressBar progressCode;
    public CodeScanner codeScanner;

    public BarCodeScanAdapter(Context context) {
        this.context = context;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {

        View layout=null;
        switch (position){
            case 0:
                layout = View.inflate(context, R.layout.item_barcode_pager_1, null);
                break;
            case 1:
                layout = View.inflate(context, R.layout.item_barcode_pager_2, null);
                scannerView=(CodeScannerView)layout.findViewById(R.id.scanner_view);
                initBarCodeScanner(scannerView);
                break;
            case 2:
                layout = View.inflate(context, R.layout.item_barcode_pager_3, null);
                editBarCode=(TextInputEditText)layout.findViewById(R.id.edit_code);
                textBarcodeError=(TextView)layout.findViewById(R.id.text_barcode_error);
                progressCode=(ProgressBar)layout.findViewById(R.id.progress_bar);
                initInputBarcodeListener(editBarCode);
                break;
        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position,@NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view,@NonNull Object object) {
        return view == object;
    }

    private void initBarCodeScanner(CodeScannerView scannerView){
        codeScanner = new CodeScanner(context, scannerView);
        codeScanner.setFormats(CodeScanner.ONE_DIMENSIONAL_FORMATS);
        codeScanner.setScanMode(ScanMode.SINGLE);
        codeScanner.startPreview();
        codeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull Result result) {
                EventBus.getDefault().post(new BarCodeScanEvent(result.getText()));
            }
        });
    }

    private void initInputBarcodeListener(TextInputEditText editBarCode){
        editBarCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==13){
                    progressCode.setVisibility(View.VISIBLE);
                    EventBus.getDefault().post(new BarCodeScanEvent(editable.toString()));
                }else{
                    textBarcodeError.setText("");
                }
            }
        });
    }

    public void showCodeError(String error){
        progressCode.setVisibility(View.GONE);
        textBarcodeError.setText(error);
    }

}
