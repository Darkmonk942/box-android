package com.virtualeducation.box.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.virtualeducation.box.ApplicationBox;
import com.virtualeducation.box.R;
import com.virtualeducation.box.event.GetPackageEvent;
import com.virtualeducation.box.model.PackageRetailModel;
import com.virtualeducation.box.network.ApiClient;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarCodeViewModel extends ViewModel {
    public MutableLiveData<PackageRetailModel> packageRetailData=new MutableLiveData<>();
    public MutableLiveData<PackageRetailModel> packageRetailDataWithCaptcha=new MutableLiveData<>();

    public String barcode;

    public void getPackageByBarcode(String barcode){
        this.barcode=barcode;
        ApiClient.getClient().getPackageInfo(barcode).enqueue(new Callback<PackageRetailModel>() {
            @Override
            public void onResponse(Call<PackageRetailModel> call, Response<PackageRetailModel> response) {
                if(response.isSuccessful()){
                    packageRetailData.setValue(response.body());
                }else{
                    PackageRetailModel packageRetailModel=new PackageRetailModel();
                    packageRetailModel.error= ApplicationBox.context.getString(R.string.error_barcode_disable);
                    //EventBus.getDefault().post(new GetPackageEvent(packageRetailModel));
                    packageRetailData.setValue(packageRetailModel);
                }
            }

            @Override
            public void onFailure(Call<PackageRetailModel> call, Throwable t) {
                PackageRetailModel packageRetailModel=new PackageRetailModel();
                packageRetailModel.error= ApplicationBox.context.getString(R.string.error_internet);
                //EventBus.getDefault().post(new GetPackageEvent(packageRetailModel));
                packageRetailData.setValue(packageRetailModel);
            }
        });

    }

    public void getPackageByBarcode(String barcode,String captcha){
        this.barcode=barcode;
        ApiClient.getClient().getPackageInfo(barcode,captcha).enqueue(new Callback<PackageRetailModel>() {
            @Override
            public void onResponse(Call<PackageRetailModel> call, Response<PackageRetailModel> response) {
                if(response.isSuccessful()){
                    packageRetailDataWithCaptcha.setValue(response.body());
                }else{
                    PackageRetailModel packageRetailModel=new PackageRetailModel();
                    packageRetailModel.error= ApplicationBox.context.getString(R.string.error_barcode_disable);
                    packageRetailData.setValue(packageRetailModel);
                }
            }

            @Override
            public void onFailure(Call<PackageRetailModel> call, Throwable t) {
                PackageRetailModel packageRetailModel=new PackageRetailModel();
                packageRetailModel.error= ApplicationBox.context.getString(R.string.error_internet);
                packageRetailData.setValue(packageRetailModel);
            }
        });

    }

}
