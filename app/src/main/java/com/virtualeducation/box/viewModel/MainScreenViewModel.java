package com.virtualeducation.box.viewModel;

import android.animation.Animator;
import android.arch.lifecycle.ViewModel;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.virtualeducation.box.ApplicationBox;
import com.virtualeducation.box.LocalStorage;
import com.virtualeducation.box.R;
import com.virtualeducation.box.event.AlertDialogEvent;
import com.virtualeducation.box.event.DownloadAppEvent;
import com.virtualeducation.box.event.DownloadComplete;
import com.virtualeducation.box.event.ExitFromPackageEvent;
import com.virtualeducation.box.event.GetActivationKeyEvent;
import com.virtualeducation.box.event.LinkGooglePlayEvent;
import com.virtualeducation.box.event.SetLocaleEvent;
import com.virtualeducation.box.model.AppModel;
import com.virtualeducation.box.model.FeedbackModel;
import com.virtualeducation.box.network.ApiClient;
import com.virtualeducation.box.network.ApiClientProgress;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainScreenViewModel extends ViewModel {
    public static String LANGUAGE_RUSSIAN = "ru";
    public static String LANGUAGE_UKRAINE = "ua";

    private String[] languages= {LANGUAGE_RUSSIAN, LANGUAGE_UKRAINE};
    private LayoutInflater layoutInflater;
    private Context context;
    private AtomicBoolean isFirstGoInstall = new AtomicBoolean();

    public void init(LayoutInflater layoutInflater, Context context){
        this.layoutInflater=layoutInflater;
        this.context = context;
    }

    private void sendFeedback(FeedbackModel feedbackModel){
        ApiClient.getClient().sendFeedbackRequest(feedbackModel).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    createToast("Сообщение успешно отправлено");
                }else{
                    createToast("Произошла ошибка");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                createToast("Произошла ошибка");
            }
        });
    }

    public void getApkFile(final Context context, String url, final AppModel appModel){
        ApiClientProgress.getClient().downloadFile(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                EventBus.getDefault().postSticky(new DownloadComplete());
                File file = new File(context.getFilesDir(),"boxapp"+appModel.id);
                InputStream inputStream=response.body().byteStream();
                OutputStream output=null;
                try{
                    output = new FileOutputStream(file);
                    byte[] buffer = new byte[1024]; // or other buffer size
                    int read;

                    while ((read = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }

                    output.flush();
                    inputStream.close();
                    output.close();
                    appModel.localFilePath = file.getAbsolutePath();

                    LocalStorage.saveAppLink(appModel.id, file.getAbsolutePath());
                    if(!isFirstGoInstall.get()){
                        openApk(file);
                        isFirstGoInstall.set(true);
                    }


                }catch (Exception e){
                    Log.d("ерор",e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public AlertDialog getDialog(Context context, String dialogType,String key, String appName){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.DialogTheme);
        View dialogView = layoutInflater.inflate(getDialogLayout(dialogType), null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        final FrameLayout frameBg=(FrameLayout) dialogView.findViewById(R.id.frame_bg);
        frameBg.post(new Runnable() {
            @Override
            public void run() {
                int endRadius = (int) Math.hypot(frameBg.getMeasuredWidth(), frameBg.getMeasuredHeight());

                Animator anim = ViewAnimationUtils.createCircularReveal(frameBg,
                        0, 0, 0, endRadius);
                anim.setDuration(600);

                frameBg.setVisibility(View.VISIBLE);
                anim.start();
            }
        });

        switch (dialogType){
            case AlertDialogEvent.DIALOG_GET_KEY:
                TextView textNo=(TextView)dialogView.findViewById(R.id.text_no);
                TextView textOk=(TextView)dialogView.findViewById(R.id.text_ok);
                TextView textView=(TextView)dialogView.findViewById(R.id.title_about);
                ImageButton btnKeyGetExit=(ImageButton)dialogView.findViewById(R.id.ic_close);
                btnKeyGetExit.setOnClickListener(dialogClickClose);
                textView.setText(context.getString(R.string.main_alert_text_approve_key) +" "+appName);
                textNo.setOnClickListener(dialogClickClose);
                textOk.setOnClickListener(dialogClickGetCode);
                break;
            case AlertDialogEvent.DIALOG_MENU:
                FrameLayout frameExit=(FrameLayout)dialogView.findViewById(R.id.frame_exit);
                FrameLayout frameFeedback=(FrameLayout)dialogView.findViewById(R.id.frame_feedback);
                RelativeLayout spoilerFeedback=(RelativeLayout)dialogView.findViewById(R.id.frame_spoiler_feedback);
                TextInputEditText editTitle=(TextInputEditText)dialogView.findViewById(R.id.edit_name);
                TextInputEditText editEmail=(TextInputEditText)dialogView.findViewById(R.id.edit_email);
                TextInputEditText editMessage=(TextInputEditText)dialogView.findViewById(R.id.edit_message);
                TextView btnSendFeedback=(TextView)dialogView.findViewById(R.id.btn_send);
                ImageButton btnMenuExit=(ImageButton)dialogView.findViewById(R.id.ic_close);
                AppCompatSpinner languageSpinner = (AppCompatSpinner)dialogView.findViewById(R.id.spinner_language);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                        R.layout.item_spinner_language, languages);
                languageSpinner.setAdapter(adapter);
                initSetLanguageListener(languageSpinner);
                frameExit.setOnClickListener(dialogClickExit);
                btnMenuExit.setOnClickListener(dialogClickClose);
                frameFeedback.setTag(spoilerFeedback);
                frameFeedback.setOnClickListener(dialogClickShowFeedback);
                initFeedbackClickListener(editTitle,editEmail,editMessage,btnSendFeedback,spoilerFeedback);
                break;
            case AlertDialogEvent.DIALOG_SHOW_KEY:
                ImageButton btnKeyShowExit=(ImageButton)dialogView.findViewById(R.id.ic_close);
                TextView textKey=(TextView)dialogView.findViewById(R.id.text_activation_key);
                LinearLayout linearCopy=(LinearLayout)dialogView.findViewById(R.id.linear_copy);
                textKey.setText(key);
                linearCopy.setTag(key);
                btnKeyShowExit.setOnClickListener(dialogClickClose);
                linearCopy.setOnClickListener(dialogClickCopyCode);
                break;
            case AlertDialogEvent.DIALOG_APP:
                ImageButton btnCloseApp=(ImageButton)dialogView.findViewById(R.id.ic_close);
                FrameLayout frameAppGooglePlay=(FrameLayout)dialogView.findViewById(R.id.frame_google_play);
                FrameLayout frameAppKey=(FrameLayout)dialogView.findViewById(R.id.frame_key);
                frameAppGooglePlay.setOnClickListener(dialogClickGooglePlay);
                btnCloseApp.setOnClickListener(dialogClickClose);
                break;
        }

        return alertDialog;
    }

    public int getDialogLayout(String dialogType){
        switch (dialogType){
            case AlertDialogEvent.DIALOG_GET_KEY:
                return R.layout.alert_get_key_approve;
            case AlertDialogEvent.DIALOG_MENU:
                return R.layout.alert_menu;
            case AlertDialogEvent.DIALOG_SHOW_KEY:
                return R.layout.alert_show_app_key;
            case AlertDialogEvent.DIALOG_APP:
                return R.layout.alert_about_app;
        }
        return R.layout.alert_menu;
    }

    private void initSetLanguageListener(AppCompatSpinner spinner){
        if(LocalStorage.getUserLanguage().equals(MainScreenViewModel.LANGUAGE_UKRAINE)){
            spinner.setSelection(1);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String language = ((TextView)view).getText().toString().toLowerCase();
                if(LocalStorage.getUserLanguage().equals(language)){
                    return;
                }
                LocalStorage.setUserLanguage(language);
                EventBus.getDefault().post(new SetLocaleEvent());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initFeedbackClickListener(final TextInputEditText editName, final TextInputEditText editEmail,
                                           final TextInputEditText editMessage, TextView btnSend, final RelativeLayout frameFeedback){

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FeedbackModel feedbackModel=new FeedbackModel();
                feedbackModel.email=editEmail.getText().toString();
                feedbackModel.message=editMessage.getText().toString();
                feedbackModel.title=editName.getText().toString();

                if(feedbackModel.title.trim().length()<2){
                    createToast("Введите ваше имя");
                    return;
                }
                if(feedbackModel.message.trim().length()<3){
                    createToast("Введите ваше сообщение");
                    return;
                }
                if(feedbackModel.email.trim().length()<6||!feedbackModel.email.contains("@")||!feedbackModel.email.contains(".")){
                    createToast("Введите корректный email");
                    return;
                }

                editName.setText("");
                editEmail.setText("");
                editMessage.setText("");
                frameFeedback.setVisibility(View.GONE);
                sendFeedback(feedbackModel);
            }
        });
    }

    private View.OnClickListener dialogClickClose=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EventBus.getDefault().post(new AlertDialogEvent(""));
        }
    };

    private View.OnClickListener dialogClickExit=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EventBus.getDefault().post(new ExitFromPackageEvent());
        }
    };

    private View.OnClickListener dialogClickGetCode=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AppModel appModel=(AppModel)view.getTag();
            EventBus.getDefault().post(new GetActivationKeyEvent(appModel.keyId, appModel));
        }
    };

    private View.OnClickListener dialogClickGooglePlay=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AppModel appModel=(AppModel)view.getTag();
            EventBus.getDefault().post(new LinkGooglePlayEvent(appModel.urlGooglePlay));
        }
    };

    private View.OnClickListener dialogClickShowFeedback=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ViewGroup viewGroup=(ViewGroup)view.getTag();
            if(viewGroup.getVisibility()==View.VISIBLE){
                viewGroup.setVisibility(View.GONE);
            }else{
                viewGroup.setVisibility(View.VISIBLE);
            }
        }
    };

    private View.OnClickListener dialogClickCopyCode=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ClipboardManager clipboard = (ClipboardManager)ApplicationBox.context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Copy text", view.getTag().toString());
            clipboard.setPrimaryClip(clip);

            createToast(ApplicationBox.context.getString(R.string.toast_key_copy));
        }
    };

    public void openApk(File file) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            file.setReadable(true, false);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            context.getApplicationContext().startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            Uri fileUri = FileProvider.getUriForFile(context,
                    "com.virtualeducation.box",
                    file);

            intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            context.startActivity(intent);
        }
    }

    private void createToast(String text){
        Toast.makeText(ApplicationBox.context, text,
                Toast.LENGTH_LONG).show();
    }
}
