package com.virtualeducation.box;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.orhanobut.hawk.Hawk;

import java.util.Locale;

public class ApplicationBox extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getBaseContext();

        // Init encrypt local storage library
        Hawk.init(this).build();
    }



}
