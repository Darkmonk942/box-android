package com.virtualeducation.box.model;

import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class AppInfoDialog {
    public TextView textProgress;
    public ImageView icDownload;
    public AppModel appModel;

    public AppInfoDialog(TextView textProgress, ImageView icDownload, AppModel appModel){
        this.textProgress = textProgress;
        this.icDownload = icDownload;
        this.appModel = appModel;
    }
}
