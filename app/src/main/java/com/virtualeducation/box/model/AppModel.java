package com.virtualeducation.box.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppModel {

    @SerializedName("categoryName")
    @Expose
    public String categoryName;

    @SerializedName("iconUrl")
    @Expose
    public String urlIcon;

    @SerializedName("logoUrl")
    @Expose
    public String urlLogo;

    @SerializedName("boxUrl")
    @Expose
    public String downloadUrl;

    @SerializedName("googlePlayUrl")
    @Expose
    public String urlGooglePlay;

    @SerializedName("primeColor")
    @Expose
    public String bgColor;

    @SerializedName("version")
    @Expose
    public String version;

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("keyDataBaseID")
    @Expose
    public int keyId;

    @SerializedName("name")
    @Expose
    public String appName;

    @SerializedName("description")
    @Expose
    public String appDescription;

    @SerializedName("isFree")
    @Expose
    public boolean isFree;

    public String localFilePath;
}
