package com.virtualeducation.box.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RetailModel {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String retailName;

    @SerializedName("text")
    @Expose
    public String retailAbout;

    @SerializedName("logoURL")
    @Expose
    public String urlLogo;

    @SerializedName("primeColor")
    @Expose
    public String colorPrimeHex;

    @SerializedName("alternativeColor")
    @Expose
    public String colorAlterHex;
}
