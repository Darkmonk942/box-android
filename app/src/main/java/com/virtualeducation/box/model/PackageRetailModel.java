package com.virtualeducation.box.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PackageRetailModel {

    @SerializedName("needReCaptcha")
    @Expose
    public boolean needRecaptcha;

    @SerializedName("retailer")
    @Expose
    public RetailModel retailModel;

    @SerializedName("package")
    @Expose
    public PackageInfoModel infoModel;

    @SerializedName("categories")
    @Expose
    public ArrayList<AppCategoryModel> categories;

    public String error="";
}
