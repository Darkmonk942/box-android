package com.virtualeducation.box.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AppCategoryModel {

    @SerializedName("applications")
    @Expose
    public ArrayList<AppModel> apps;

    @SerializedName("activeApp")
    @Expose
    public ActiveAppInfo activationKeyModel;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("hasActiveApp")
    @Expose
    public boolean isActivated;

    @SerializedName("isPaid")
    @Expose
    public boolean isPaid;
}
