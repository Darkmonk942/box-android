package com.virtualeducation.box.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivatedAppModel {

    @SerializedName("key")
    @Expose
    public String key;

    @SerializedName("keyDataBaseID")
    @Expose
    public int keyDBId;
}
