package com.virtualeducation.box.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActiveAppInfo {

    @SerializedName("key")
    @Expose
    public String key;

    @SerializedName("keyDataBaseID")
    @Expose
    public int keyDBid;
}
