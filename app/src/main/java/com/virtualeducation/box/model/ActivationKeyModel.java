package com.virtualeducation.box.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivationKeyModel {

    @SerializedName("keyValue")
    @Expose
    public String key;

    @SerializedName("keyDataBaseID")
    @Expose
    public int keyDBid;

    @SerializedName("package")
    @Expose
    public PackageRetailModel packageRetailModel;
}
