package com.virtualeducation.box;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.virtualeducation.box.activity.BarCodeScanActivity;
import com.virtualeducation.box.activity.MainScreenActivity;
import com.virtualeducation.box.activity.StartScreenActivity;

import java.util.Locale;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserLanguage();

        if(LocalStorage.isPackageWasScan()){
            startActivity(new Intent(this, MainScreenActivity.class));
        }else{
            startActivity(new Intent(this,StartScreenActivity.class));
        }
        finish();
    }

    private void setUserLanguage(){
        if(LocalStorage.getUserLanguage() == null){
            if(Locale.getDefault().getLanguage().equals("uk") || Locale.getDefault().getLanguage().equals("ru")){
                LocalStorage.setUserLanguage(Locale.getDefault().getLanguage());
            }else{
                LocalStorage.setUserLanguage("ru");
            }
        }
        Log.d("язык",LocalStorage.getUserLanguage());
        Locale locale = new Locale(LocalStorage.getUserLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
