package com.virtualeducation.box.network;

import com.virtualeducation.box.model.ActivationKeyModel;
import com.virtualeducation.box.model.PackageRetailModel;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface Api {

    // Get package information from barcode ReCaptcha
    @Headers("Content-Type: application/json")
    @POST("barcode")
    Call<PackageRetailModel> getPackageInfo(@Header("language") String language, @Body Map<String,String> barcode);

    @Headers("Content-Type: application/json")
    @POST("barcode")
    Call<PackageRetailModel> getPackageInfoWithCaptcha(@Body Map<String,String> requestMap);

    // Get activation key by barcode and app id
    @Headers("Content-Type: application/json")
    @PUT("key/activation")
    Call<ActivationKeyModel> getActivationKey(@Body Map<String,String> bodyKey);

    // Send feedback from user to server
    @Headers("Content-Type: application/json")
    @POST("feedback")
    Call<Void> sendFeedback(@Body Map<String,String> bodyKey);

    // Get apk file from server for install app
    @Streaming
    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);
}
