package com.virtualeducation.box.network;

import android.util.Log;

import com.google.gson.Gson;
import com.virtualeducation.box.LocalStorage;
import com.virtualeducation.box.event.ProgressDownloadEvent;
import com.virtualeducation.box.model.ActivationKeyModel;
import com.virtualeducation.box.model.AppCategoryModel;
import com.virtualeducation.box.model.FeedbackModel;
import com.virtualeducation.box.model.PackageRetailModel;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final String BASE_URL="https://webadmin.box.staging.dbvirtualeducation.com/api/1.0/";

    private Api apiRequests;
    private static ApiClient getClient = null;
    private Retrofit retrofit;

    private ApiClient(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        final DownloadProgressListener downloadProgressListener=new DownloadProgressListener() {
            @Override
            public void update(long bytesRead, long contentLength, boolean done) {
                EventBus.getDefault().post(new ProgressDownloadEvent(bytesRead,contentLength,done));
            }
        };

        OkHttpClient networkClient = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(networkClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.apiRequests=retrofit.create(Api.class);
    }

    public static ApiClient getClient() {
        if (getClient == null) {
            getClient = new ApiClient();
        }
        return getClient;
    }

    public Call<PackageRetailModel> getPackageInfo(String barcode){
        Map<String,String> requestMap=new HashMap<>();
        requestMap.put("barcode",barcode);
        return apiRequests.getPackageInfo(LocalStorage.getUserLanguage().toLowerCase(),requestMap);
    }

    public Call<PackageRetailModel> getPackageInfo(String barcode,String captcha){
        Map<String,String> requestMap=new HashMap<>();
        requestMap.put("barcode",barcode);
        requestMap.put("response",captcha);

        return apiRequests.getPackageInfo(LocalStorage.getUserLanguage().toLowerCase(),requestMap);
    }

    public Call<Void> sendFeedbackRequest(FeedbackModel feedbackModel){
        Map<String,String> requestMap=new HashMap<>();
        requestMap.put("title",feedbackModel.title);
        requestMap.put("message",feedbackModel.message);
        requestMap.put("email",feedbackModel.email);

        return apiRequests.sendFeedback(requestMap);
    }

    public Call<ActivationKeyModel> getActivationKey(int appId){
        Map<String,String> requestMap=new HashMap<>();
        requestMap.put("barCode", LocalStorage.getUserBarcode());
        requestMap.put("keyDatabaseID", String.valueOf(appId));

        return apiRequests.getActivationKey(requestMap);
    }
}
