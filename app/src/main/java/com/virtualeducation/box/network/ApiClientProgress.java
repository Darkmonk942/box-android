package com.virtualeducation.box.network;

import com.virtualeducation.box.event.ProgressDownloadEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientProgress {

    private static final String BASE_URL="http://116.203.31.57/api/1.0/";

    private Api apiRequests;
    private static ApiClientProgress getClient = null;
    private Retrofit retrofit;

    private ApiClientProgress(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        final DownloadProgressListener downloadProgressListener=new DownloadProgressListener() {
            @Override
            public void update(long bytesRead, long contentLength, boolean done) {
                EventBus.getDefault().post(new ProgressDownloadEvent(bytesRead,contentLength,done));
            }
        };

        OkHttpClient networkClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addNetworkInterceptor(new Interceptor() {
                    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
                        Response originalResponse = chain.proceed(chain.request());
                        return originalResponse.newBuilder()
                                .body(new ProgressBody(originalResponse.body(), downloadProgressListener))
                                .build();
                    }
                })
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(networkClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.apiRequests=retrofit.create(Api.class);
    }

    public static ApiClientProgress getClient() {
        if (getClient == null) {
            getClient = new ApiClientProgress();
        }
        return getClient;
    }

    public Call<ResponseBody> downloadFile(String url){
        return apiRequests.downloadFile(url);
    }
}
