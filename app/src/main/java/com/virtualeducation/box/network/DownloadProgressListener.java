package com.virtualeducation.box.network;

public interface DownloadProgressListener {
    void update(long bytesRead, long contentLength, boolean done);
}
