package com.virtualeducation.box;

import com.orhanobut.hawk.Hawk;
import com.virtualeducation.box.model.PackageRetailModel;
import com.virtualeducation.box.viewModel.MainScreenViewModel;

import java.util.HashMap;
import java.util.TreeMap;

public class LocalStorage {
    private static final String PACKAGE_RETAIL="package_model";
    private static final String USER_BARCODE="user_barcode";
    private static final String USER_LANGUAGE="user_language";
    private static final String APPS_LINKS="apps_links";

    // Get\Set retail package info
    public static PackageRetailModel getRetailPackage(){
        return Hawk.get(PACKAGE_RETAIL,new PackageRetailModel());
    }

    public static void setRetailPackage(PackageRetailModel packageRetailModel){
        Hawk.put(PACKAGE_RETAIL,packageRetailModel);
    }

    public static Boolean isPackageWasScan(){
        return Hawk.contains(PACKAGE_RETAIL);
    }

    // Get\Set user barcode
    public static String getUserBarcode(){
        return Hawk.get(USER_BARCODE,"");
    }

    public static void setUserBarcode(String barcode){
        Hawk.put(USER_BARCODE,barcode);
    }

    // Get\Set user language
    public static String getUserLanguage(){
        return Hawk.get(USER_LANGUAGE, null);
    }

    public static void setUserLanguage(String language){
        if (language.equals("uk")){
            Hawk.put(USER_LANGUAGE, "ua");
            return;
        }
        Hawk.put(USER_LANGUAGE, language);
    }

    // Totally clear DB
    public static void clearBase(){
        String language = getUserLanguage();
        Hawk.deleteAll();
        setUserLanguage(language);
    }

    public static void saveAppLink(int appId, String link){
        final HashMap<Integer, String> appsMap = getAppsLinks();
        appsMap.put(appId, link);
        Hawk.put(APPS_LINKS, appsMap);
    }

    public static HashMap<Integer, String> getAppsLinks(){
        return Hawk.get(APPS_LINKS, new HashMap<Integer, String>());
    }
}
