package com.virtualeducation.box.activity;

import android.Manifest;
import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.virtualeducation.box.R;
import com.virtualeducation.box.databinding.ActivityStartScreenBinding;

import java.util.Locale;

public class StartScreenActivity extends AppCompatActivity {
    private ActivityStartScreenBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding=DataBindingUtil.setContentView(this, R.layout.activity_start_screen);

        requestPermission();
        initVideoView();
        startFadeAnimation();
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
    }

    private void initVideoView(){
        activityBinding.videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() +"/"+R.raw.video_start_screen));
        activityBinding.videoView.start();

        activityBinding.videoView.setOnPreparedListener (new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
    }

    private void startFadeAnimation(){
        activityBinding.textAbout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade));
        activityBinding.textCloud.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade));
        activityBinding.icLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade));
    }

    public void startAppClick(View v){
        //startActivity(new Intent(this,MainScreenActivity.class));
        startActivity(new Intent(this,BarCodeScanActivity.class));
        finish();
    }


}
