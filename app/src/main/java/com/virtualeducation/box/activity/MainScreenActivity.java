package com.virtualeducation.box.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.virtualeducation.box.LocalStorage;
import com.virtualeducation.box.R;
import com.virtualeducation.box.adapter.RecyclerCategoryAdapter;
import com.virtualeducation.box.databinding.ActivityMainScreenBinding;
import com.virtualeducation.box.event.AboutAppEvent;
import com.virtualeducation.box.event.AlertDialogEvent;
import com.virtualeducation.box.event.DownloadAppEvent;
import com.virtualeducation.box.event.DownloadComplete;
import com.virtualeducation.box.event.ExitFromPackageEvent;
import com.virtualeducation.box.event.GetActivationKeyEvent;
import com.virtualeducation.box.event.GetKeyEvent;
import com.virtualeducation.box.event.GetPackageEvent;
import com.virtualeducation.box.event.LinkGooglePlayEvent;
import com.virtualeducation.box.event.ProgressDownloadEvent;
import com.virtualeducation.box.event.SetLocaleEvent;
import com.virtualeducation.box.model.ActivationKeyModel;
import com.virtualeducation.box.model.ActiveAppInfo;
import com.virtualeducation.box.model.AppCategoryModel;
import com.virtualeducation.box.model.AppInfoDialog;
import com.virtualeducation.box.model.AppModel;
import com.virtualeducation.box.model.PackageRetailModel;
import com.virtualeducation.box.network.ApiClient;
import com.virtualeducation.box.viewModel.BarCodeViewModel;
import com.virtualeducation.box.viewModel.MainScreenViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainScreenActivity extends AppCompatActivity {
    private ActivityMainScreenBinding activityBinding;
    private RecyclerCategoryAdapter categoryAdapter;
    private PackageRetailModel packageRetailModel;

    private MainScreenViewModel viewModel;
    private BarCodeViewModel barCodeViewModel;

    private AlertDialog alertDialog;
    private AlertDialog alertDialogAdd;
    private AppInfoDialog appInfoDialog;

    private ArrayList<AppCategoryModel> categories = new ArrayList<>();
    private ArrayList<DownloadAppEvent> downloadQueue = new ArrayList<>();
    private AtomicInteger currentDownloadEvent = new AtomicInteger();
    private AtomicBoolean isApkDownloading = new AtomicBoolean();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setStatusBarColor(LocalStorage.getRetailPackage().retailModel.colorPrimeHex);
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_screen);
        barCodeViewModel = ViewModelProviders.of(this).get(BarCodeViewModel.class);
        viewModel = ViewModelProviders.of(this).get(MainScreenViewModel.class);
        viewModel.init(getLayoutInflater(), this);
        packageRetailModel = LocalStorage.getRetailPackage();
        categories = packageRetailModel.categories;

        setRetailTheme(packageRetailModel);
        //initCategoryRecycler(categories);
        getPackage();
        initDataListener();
    }

    private void getPackage(){
        barCodeViewModel.getPackageByBarcode(LocalStorage.getUserBarcode());
    }

    private void initDataListener(){
        barCodeViewModel.packageRetailData.observe(this, new Observer<PackageRetailModel>() {
            @Override
            public void onChanged(@Nullable PackageRetailModel mPackageRetailModel) {
                if(packageRetailModel != null){
                    LocalStorage.setRetailPackage(mPackageRetailModel);
                    packageRetailModel = mPackageRetailModel;
                    setRetailTheme(packageRetailModel);
                    categories.clear();
                    categories.addAll(packageRetailModel.categories);
                    initCategoryRecycler(categories);
                    //barCodeViewModel.packageRetailData.setValue(null);
                }
            }
        });
    }

    private void setRetailTheme(PackageRetailModel packageRetailModel) {
        if (packageRetailModel.retailModel.urlLogo == null) {
            return;
        }
        activityBinding.frameHeader.setBackgroundColor(Color.parseColor(packageRetailModel.retailModel.colorPrimeHex));
        activityBinding.getRoot().setBackgroundColor(Color.parseColor(packageRetailModel.retailModel.colorPrimeHex));
        Glide.with(this)
                .load(packageRetailModel.retailModel.urlLogo)
                .into(activityBinding.imgLogo);
    }

    private void initCategoryRecycler(ArrayList<AppCategoryModel> categories) {

        setDownloadFilePath(categories);
        categoryAdapter = new RecyclerCategoryAdapter(this, categories);
        activityBinding.recyclerCategory.setLayoutManager(new LinearLayoutManager(this));
        activityBinding.recyclerCategory.setHasFixedSize(true);
        activityBinding.recyclerCategory.setAdapter(categoryAdapter);
    }

    private void setDownloadFilePath(ArrayList<AppCategoryModel> categories){
        HashMap<Integer, String> savedPath = LocalStorage.getAppsLinks();
        for (AppCategoryModel appCategoryModel : categories){
            for (AppModel appModel : appCategoryModel.apps){
                if(savedPath.containsKey(appModel.id)){
                    appModel.localFilePath = savedPath.get(appModel.id);
                }
            }
        }
    }

    public void menuClick(View v) {
        alertDialog = viewModel.getDialog(this, AlertDialogEvent.DIALOG_MENU, null, null);
        alertDialog.show();
        TextView textPackageName = (TextView) alertDialog.findViewById(R.id.text_package_name);
        try {
            textPackageName.setText(packageRetailModel.infoModel.name);
        } catch (Exception e) {
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void startDownloadClick(DownloadAppEvent event) {
        if(event.appModel.localFilePath != null){
            Log.d("апка",event.appModel.localFilePath);
            viewModel.openApk(new File(event.appModel.localFilePath));
            return;
        }
        for (DownloadAppEvent appEvent : downloadQueue){
            if (appEvent.appModel.id == event.appModel.id){
                return;
            }
        }
        downloadQueue.add(event);
        event.textDownloadProgress.setVisibility(View.VISIBLE);
        event.icDownload.setVisibility(View.GONE);
        event.textDownloadProgress.setText("0%");
        if(!isApkDownloading.get()){
            viewModel.getApkFile(this, event.appModel.downloadUrl, event.appModel);
            isApkDownloading.set(true);
            currentDownloadEvent.incrementAndGet();
        }
    }

    // Start download apk from server
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getDownloadAppProgress(ProgressDownloadEvent event) {
        //Log.d("Загрузка", String.valueOf(event.currentBytes));
        if(currentDownloadEvent.get() == 0){
            isApkDownloading.set(false);
            downloadQueue.clear();
            return;
        }
        DownloadAppEvent appEvent = downloadQueue.get(currentDownloadEvent.get()-1);

        if (appEvent != null) {
            float progress = ((float) event.currentBytes / event.totalBytes) * 100;
            if(appEvent.fromDialog){
                ViewGroup group =  (ViewGroup) activityBinding.recyclerCategory.findViewHolderForAdapterPosition(appEvent.holderPosition).itemView;
                ImageView icDownload = group.findViewById(R.id.ic_download);
                TextView textProgress = group.findViewById(R.id.text_download_progress);

                icDownload.setVisibility(View.GONE);
                textProgress.setVisibility(View.VISIBLE);
                textProgress.setText(String.valueOf(Math.round(progress)) + "%");
            }

            appEvent.textDownloadProgress.setVisibility(View.VISIBLE);
            appEvent.icDownload.setVisibility(View.GONE);

            appEvent.textDownloadProgress.setText(String.valueOf(Math.round(progress)) + "%");

            if(appInfoDialog != null && appInfoDialog.appModel.id == appEvent.appModel.id){
                appInfoDialog.icDownload.setVisibility(View.GONE);
                appInfoDialog.textProgress.setVisibility(View.VISIBLE);
                appInfoDialog.textProgress.setText(String.valueOf(Math.round(progress)) + "%");
            }
        }
        if (event.isDone) {

//            if(appEvent.fromDialog){
//                ViewGroup group =  (ViewGroup) activityBinding.recyclerCategory.findViewHolderForAdapterPosition(appEvent.holderPosition).itemView;
//                ImageView icDownload = group.findViewById(R.id.ic_download);
//                TextView textProgress = group.findViewById(R.id.text_download_progress);
//
//                textProgress.setVisibility(View.GONE);
//                icDownload.setVisibility(View.VISIBLE);
//                icDownload.setImageResource(R.drawable.ic_download_complete);
//            }
//
//            appEvent.textDownloadProgress.setVisibility(View.GONE);
//            appEvent.icDownload.setVisibility(View.VISIBLE);
//            appEvent.icDownload.setImageResource(R.drawable.ic_download_complete);
//            if(currentDownloadEvent.get() < downloadQueue.size()){
//                final DownloadAppEvent newAPpEvent  = downloadQueue.get(currentDownloadEvent.getAndIncrement());
//                viewModel.getApkFile(this, newAPpEvent.appModel.downloadUrl, newAPpEvent.appModel);
//            }else{
//                isApkDownloading.set(false);
//                currentDownloadEvent.set(0);
//                downloadQueue.clear();
//            }
//
//            if(appInfoDialog != null && appInfoDialog.appModel.id == appEvent.appModel.id){
//                appInfoDialog.icDownload.setVisibility(View.VISIBLE);
//                appInfoDialog.icDownload.setImageResource(R.drawable.ic_download_complete_white);
//                appInfoDialog.textProgress.setVisibility(View.GONE);
//            }
//
//            for (int i =0; i<currentDownloadEvent.get()-1;i++){
//                final DownloadAppEvent checkAppEvent  = downloadQueue.get(i);
//                checkAppEvent.icDownload.setVisibility(View.VISIBLE);
//                checkAppEvent.icDownload.setImageResource(R.drawable.ic_download_complete);
//                checkAppEvent.textDownloadProgress.setVisibility(View.GONE);
//            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void isDownloadComplete(DownloadComplete downloadComplete){
        DownloadAppEvent appEvent = downloadQueue.get(currentDownloadEvent.get()-1);

        if(appEvent.fromDialog){
            ViewGroup group =  (ViewGroup) activityBinding.recyclerCategory.findViewHolderForAdapterPosition(appEvent.holderPosition).itemView;
            ImageView icDownload = group.findViewById(R.id.ic_download);
            TextView textProgress = group.findViewById(R.id.text_download_progress);

            textProgress.setVisibility(View.GONE);
            icDownload.setVisibility(View.VISIBLE);
            icDownload.setImageResource(R.drawable.ic_download_complete);
        }

        appEvent.textDownloadProgress.setVisibility(View.GONE);
        appEvent.icDownload.setVisibility(View.VISIBLE);
        appEvent.icDownload.setImageResource(R.drawable.ic_download_complete);
        if(currentDownloadEvent.get() < downloadQueue.size()){
            final DownloadAppEvent newAPpEvent  = downloadQueue.get(currentDownloadEvent.getAndIncrement());
            viewModel.getApkFile(this, newAPpEvent.appModel.downloadUrl, newAPpEvent.appModel);
        }else{
            isApkDownloading.set(false);
            currentDownloadEvent.set(0);
            downloadQueue.clear();
        }

        if(appInfoDialog != null && appInfoDialog.appModel.id == appEvent.appModel.id){
            appInfoDialog.icDownload.setVisibility(View.VISIBLE);
            appInfoDialog.icDownload.setImageResource(R.drawable.ic_download_complete_white);
            appInfoDialog.textProgress.setVisibility(View.GONE);
        }

        for (int i =0; i<currentDownloadEvent.get()-1;i++){
            final DownloadAppEvent checkAppEvent  = downloadQueue.get(i);
            checkAppEvent.icDownload.setVisibility(View.VISIBLE);
            checkAppEvent.icDownload.setImageResource(R.drawable.ic_download_complete);
            checkAppEvent.textDownloadProgress.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showAppInfo(AboutAppEvent event) {
        alertDialog = viewModel.getDialog(this, AlertDialogEvent.DIALOG_APP, null, null);
        alertDialog.show();
        FrameLayout frameAppGooglePlay = (FrameLayout) alertDialog.findViewById(R.id.frame_google_play);
        FrameLayout frameAppKey = (FrameLayout) alertDialog.findViewById(R.id.frame_key);
        FrameLayout frameDownload=(FrameLayout)alertDialog.findViewById(R.id.frame_download);
        ImageView imageView = (ImageView) alertDialog.findViewById(R.id.img_app);
        ImageView icDownload = (ImageView) alertDialog.findViewById(R.id.ic_download);
        TextView textAppName = (TextView) alertDialog.findViewById(R.id.text_app_name);
        TextView textAppDescr = (TextView) alertDialog.findViewById(R.id.text_app_descr);
        TextView textTitle = (TextView) alertDialog.findViewById(R.id.text_title);
        TextView textDownloadProgress = (TextView) alertDialog.findViewById(R.id.text_download_progress);
        TextView textGetKey = (TextView) alertDialog.findViewById(R.id.text_my_key);
        frameAppGooglePlay.setTag(event.appModel);
        frameAppKey.setTag(event);
        textTitle.setText(event.appModel.categoryName);
        textAppName.setText("«" + event.appModel.appName + "»:");
        textAppDescr.setText(event.appModel.appDescription);
        Glide.with(this).load(event.appModel.urlLogo).into(imageView);

        frameDownload.setTag(new DownloadAppEvent(event.appModel,icDownload,textDownloadProgress,null, true, event.holderPosition));
        frameDownload.setOnClickListener(dialogClickDownloadApp);
        frameAppKey.setOnClickListener(dialogClickShowKey);

        appInfoDialog = new AppInfoDialog(textDownloadProgress, icDownload, event.appModel);
        if(event.appCategoryModel.isActivated){
            if(event.appCategoryModel.activationKeyModel.keyDBid == event.appModel.keyId){
                textGetKey.setText(getResources().getString(R.string.main_text_show_key));
            }else{
                textGetKey.setText("-");
            }
        }
        if(event.appModel.localFilePath != null){
            appInfoDialog.icDownload.setImageResource(R.drawable.ic_download_complete_white);
        }
    }

    private View.OnClickListener dialogClickDownloadApp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EventBus.getDefault().post((DownloadAppEvent)v.getTag());
        }
    };

    private View.OnClickListener dialogClickShowKey = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AboutAppEvent aboutAppEvent = (AboutAppEvent)v.getTag();
            int appId=aboutAppEvent.appModel.keyId;
            if(aboutAppEvent.appCategoryModel.isActivated){
                String key=null;
                if(aboutAppEvent.appModel.keyId==aboutAppEvent.appCategoryModel.activationKeyModel.keyDBid){
                    key=aboutAppEvent.appCategoryModel.activationKeyModel.key;
                }
                EventBus.getDefault().post(new GetKeyEvent(aboutAppEvent.appModel,appId,aboutAppEvent.appCategoryModel.isActivated,key));
            }else{
                EventBus.getDefault().post(new GetKeyEvent(aboutAppEvent.appModel,appId,aboutAppEvent.appCategoryModel.isActivated,null));
            }
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getActivationCode(final GetActivationKeyEvent event) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        ApiClient.getClient().getActivationKey(event.appId).enqueue(new Callback<ActivationKeyModel>() {
            @Override
            public void onResponse(Call<ActivationKeyModel> call, Response<ActivationKeyModel> response) {
                if (!response.isSuccessful()) {
                    createToast(getString(R.string.toast_key_absent));
                    return;
                }
                if (response.body().key == null) {
                    createToast(getString(R.string.toast_key_absent));
                    return;
                }
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }

                categories.clear();
                ArrayList<AppCategoryModel> newCategories = response.body().packageRetailModel.categories;
                setDownloadFilePath(newCategories);
                categories.addAll(newCategories);
                LocalStorage.setRetailPackage(response.body().packageRetailModel);
                categoryAdapter.notifyDataSetChanged();
                alertDialog = viewModel.getDialog(MainScreenActivity.this, AlertDialogEvent.DIALOG_SHOW_KEY, response.body().key, null);
                alertDialog.show();
            }

            @Override
            public void onFailure(Call<ActivationKeyModel> call, Throwable t) {

            }
        });
    }

    // Close AlertDialog
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void closeAlertDialog(AlertDialogEvent event) {
        if(alertDialogAdd !=null){
            alertDialogAdd.dismiss();
            alertDialogAdd = null;
            return;
        }
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setLocaleEvent(SetLocaleEvent setLocaleEvent) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        Locale locale = new Locale(LocalStorage.getUserLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        finish();
        startActivity(new Intent(this, MainScreenActivity.class));
    }

    // Show alert for getting key
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showGetKeyAlert(GetKeyEvent event) {
        if (event.isTaken) {
            if (event.key != null) {
                alertDialogAdd = viewModel.getDialog(this, AlertDialogEvent.DIALOG_SHOW_KEY, event.key, event.appModel.appName);
                alertDialogAdd.show();
            } else {
                createToast(getString(R.string.toast_key_already_taken));
                return;
            }
        } else {
            alertDialogAdd = viewModel.getDialog(this, AlertDialogEvent.DIALOG_GET_KEY, null, event.appModel.appName);
            alertDialogAdd.show();
            TextView textOk = (TextView) alertDialogAdd.findViewById(R.id.text_ok);
            textOk.setTag(event.appModel);
        }
        FrameLayout frameBg = (FrameLayout) alertDialogAdd.findViewById(R.id.frame_bg);
        if (packageRetailModel.retailModel.colorAlterHex != null) {
            frameBg.setBackgroundColor(Color.parseColor(packageRetailModel.retailModel.colorAlterHex));
        }
    }

    // Intent to Google Play app
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getGooglePlayLink(LinkGooglePlayEvent event) {
        if (event.url != null) {
            startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(event.url)));
        }
    }

    // Clear Database, exit to start screen
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void exitFromPackage(ExitFromPackageEvent event) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        LocalStorage.clearBase();
        startActivity(new Intent(this, StartScreenActivity.class));
        finish();
    }

    private void createToast(String text) {
        Toast.makeText(this, text,
                Toast.LENGTH_LONG).show();
    }

    protected void setStatusBarColor(String color) {
        getWindow().setStatusBarColor(Color.parseColor(color));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (alertDialog == null) {
            super.onBackPressed();
            return;
        }
        if (alertDialog.isShowing()) {
            alertDialog.dismiss();
        } else {
            super.onBackPressed();
        }
    }
}
