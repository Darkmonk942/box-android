package com.virtualeducation.box.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.zxing.Result;
import com.virtualeducation.box.LocalStorage;
import com.virtualeducation.box.R;
import com.virtualeducation.box.adapter.BarCodeScanAdapter;
import com.virtualeducation.box.databinding.ActivityBarcodeScanBinding;
import com.virtualeducation.box.event.BarCodeScanEvent;
import com.virtualeducation.box.model.PackageRetailModel;
import com.virtualeducation.box.viewModel.BarCodeViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.AbstractList;
import java.util.concurrent.Executor;

import androidx.annotation.NonNull;

public class BarCodeScanActivity extends AppCompatActivity {
    private final static int PAGER_POSITION_ABOUT=0;
    private final static int PAGER_POSITION_SCAN=1;
    private final static int PAGER_POSITION_MANUAL=2;

    private ActivityBarcodeScanBinding activityBinding;
    private BarCodeViewModel viewModel;
    private  BarCodeScanAdapter adapter;
    private InputMethodManager inputMethodManager;

    private boolean isCaptchaNeed;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding=DataBindingUtil.setContentView(this,R.layout.activity_barcode_scan);
        viewModel=ViewModelProviders.of(this).get(BarCodeViewModel.class);

        inputMethodManager =
                (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        //getPackageInfoByBarcode("6371247219284");
        initGetPackage();
        initPager();
    }

    public void getCaptcha(final String barcode) {
        inputMethodManager.hideSoftInputFromWindow(adapter.editBarCode.getWindowToken(), 0);
        SafetyNet.getClient(this).verifyWithRecaptcha("6LdGh4oUAAAAABVu13O5fv_xGeANKEvL7KDgZolU")
                .addOnSuccessListener(this,
                        new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                            @Override
                            public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                                // Indicates communication with reCAPTCHA service was
                                // successful.
                                String userResponseToken = response.getTokenResult();
                                if (!userResponseToken.isEmpty()) {
                                    viewModel.getPackageByBarcode(barcode,userResponseToken);
                                    Log.d("ключ",userResponseToken);
                                }
                            }
                        })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            // An error occurred when communicating with the
                            // reCAPTCHA service. Refer to the status code to
                            // handle the error appropriately.
                            ApiException apiException = (ApiException) e;
                            int statusCode = apiException.getStatusCode();
                            Log.d("ключ","Error: " +String.valueOf(statusCode));
                        } else {
                        }
                    }
                });
    }

    private void initPager(){
        adapter=new BarCodeScanAdapter(this);
        activityBinding.pager.setAdapter(adapter);
        activityBinding.pager.setPadding(60, 0, 60, 0);
        activityBinding.pager.setClipToPadding(false);
        activityBinding.pager.setPageMargin(30);
        activityBinding.pagerTabs.setupWithViewPager(activityBinding.pager);

        activityBinding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int page) {
                switch (page){
                    case PAGER_POSITION_ABOUT:
                        if(adapter.codeScanner.isPreviewActive()){
                            adapter.codeScanner.stopPreview();
                        }
                        inputMethodManager.hideSoftInputFromWindow(adapter.editBarCode.getWindowToken(), 0);
                        break;
                    case PAGER_POSITION_SCAN:
                        adapter.codeScanner.startPreview();
                        inputMethodManager.hideSoftInputFromWindow(adapter.editBarCode.getWindowToken(), 0);
                        break;
                    case PAGER_POSITION_MANUAL:
                        if(adapter.codeScanner.isPreviewActive()){
                            adapter.codeScanner.stopPreview();
                        }
                        adapter.editBarCode.requestFocus();
                        inputMethodManager.toggleSoftInputFromWindow(
                                adapter.editBarCode.getWindowToken(),
                                InputMethodManager.SHOW_FORCED, 0);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    public void scanCodeClick(View v){
        activityBinding.pager.setCurrentItem(PAGER_POSITION_SCAN,true);
    }

    public void manualCodeClick(View v){
        activityBinding.pager.setCurrentItem(PAGER_POSITION_MANUAL,true);
    }

    public void repeatScanClick(View v){
        if(!adapter.codeScanner.isPreviewActive()){
            adapter.codeScanner.startPreview();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getBarCodeEvent(BarCodeScanEvent event){
        Log.d("Запрос","старт");
        if(event.barcode!=null){
            if(isCaptchaNeed){
                getCaptcha(event.barcode);
            }else{
                viewModel.getPackageByBarcode(event.barcode);
            }

        }
    }

    private void initGetPackage(){
        viewModel.packageRetailData.observe(this, new Observer<PackageRetailModel>() {
            @Override
            public void onChanged(@Nullable PackageRetailModel packageRetailModel) {
                if(packageRetailModel.error.isEmpty()){
                    if(packageRetailModel.retailModel!=null){
                        LocalStorage.setRetailPackage(packageRetailModel);
                        LocalStorage.setUserBarcode(viewModel.barcode);
                        startActivity(new Intent(BarCodeScanActivity.this,MainScreenActivity.class));
                        finish();
                    }else{
                        isCaptchaNeed=true;
                        getCaptcha(viewModel.barcode);
                    }

                }else{
                    showError(packageRetailModel.error);
                }
            }
        });

        viewModel.packageRetailDataWithCaptcha.observe(this, new Observer<PackageRetailModel>() {
            @Override
            public void onChanged(@Nullable PackageRetailModel packageRetailModel) {
                if(packageRetailModel.error.isEmpty()){
                    if(packageRetailModel.retailModel!=null){
                        LocalStorage.setRetailPackage(packageRetailModel);
                        LocalStorage.setUserBarcode(viewModel.barcode);
                        startActivity(new Intent(BarCodeScanActivity.this,MainScreenActivity.class));
                        finish();
                    }else{
                        showPackageError();
                        isCaptchaNeed=true;
                    }

                }else{
                    showError(packageRetailModel.error);
                }
            }
        });
    }

    private void showError(String error){
        createToast(error);
        adapter.showCodeError(error);
    }

    private void showPackageError(){
        createToast(getResources().getString(R.string.error_barcode_disable));
        adapter.showCodeError(getResources().getString(R.string.error_barcode_disable));
    }

    private void createToast(String message){
        Toast.makeText(this, message,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}